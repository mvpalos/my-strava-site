import React, { Component } from 'react';

import Nav from '../../components/Nav';
import Home from '../../components/Home';
import Activities from '../../components/Activities';


import './Dashboard.scss';

class Dashboard extends React.Component {
    

    render() {
        return (
            <div>
                <section id="Nav"><Nav/></section>
                <section id="Activities"><Activities/></section>
                
            </div>
        )
    }
}

export default Dashboard;