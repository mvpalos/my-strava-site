import React, { Component } from 'react';
import { Route, Link, Switch, Redirect, withRouter } from 'react-router-dom';

import Home from '../Home';
import Activities from '../Activities';

import './Nav.scss';
import './Nav.css';


function Nav() {
  return (
    <div className="App">
      <div className="navbar">
        <Link className = "column" to= "/">Home</Link>
        <Link className = "column" to= "/">Activities</Link>
        <Link className = "column" to= "/">Stats</Link>
        <Link className = "column" to= "/">Average</Link>
        <Link className = "column" to= "/">Gear</Link>
      </div>
        <Switch>
            <Route exact path = "/" component = {Home} />
        </Switch>
    </div>
    
  );
}

export default Nav;
