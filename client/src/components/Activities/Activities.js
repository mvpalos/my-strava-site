import React, { Component } from 'react';
import axios from 'axios';

import './Activities.scss';

class Activities extends React.Component {
    
    componentWillMount(){
        axios.get('/https://www.strava.com/api/v3/activities/41757?include_all_efforts=972d41f038c390e81ea18507ba50e0a408f28bc5')
        .then(function (res) {
          console.log('SUCCESS!', res);
        })
        .catch(function (error) {
          console.log('ERROR!', error);
        });
    }

    render() {
        return (
            <div className="activitiesSection">

                <h1>Hi, I'm Jordan</h1>
                <div className="activ-description">
                    <img style={{display: 'block', marginLeft: 'auto', marginRight: 'auto', width:"30%"}} src="/elipse-circle.png"/>
                    <p>
                        I'm a twenty something year old cyclist, runner and Toronto-bound engineer.
                        <br/>
                        During the day I write code for software compaines, and at night I cycle.
                        <br/>
                        Check out my track reccord on strava.
                    </p>
                </div>
                <p>Activities</p>

            </div>
        )
    }
}

export default Activities;